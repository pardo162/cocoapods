//
//  ViewController.swift
//  cocoapods
//
//  Created by AlejandroPardina on 28/11/17.
//  Copyright © 2017 AlejandroPardina. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {
    @IBOutlet weak var nameLavel: UILabel!
    
    @IBOutlet weak var alturaLavel: UILabel!
    
    @IBOutlet weak var pesoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func consultarButto(_ sender: Any) {
        let pkid = arc4random_uniform(250)+1
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(pkid)").responseObject { (response: DataResponse<Pokemon>) in
            
            let pokemon = response.result.value
            DispatchQueue.main.async {
                self.nameLavel.text = pokemon?.name ?? "" 
                self.alturaLavel.text = "\(pokemon?.height ?? 0)"
                self.pesoLabel.text = "\(pokemon?.weight ?? 0)"
            }

        }

    }
}


