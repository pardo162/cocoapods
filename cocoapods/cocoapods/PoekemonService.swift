//
//  PoekemonService.swift
//  cocoapods
//
//  Created by AlejandroPardina on 2/1/18.
//  Copyright © 2018 AlejandroPardina. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper
import Foundation

protocol  PokemonServiceDelegate {
    func get20FirstPokemons(pokemons:[Pokemon])
}

class PokemonService {
    
    var delegate:PokemonServiceDelegate?
    
    func dowloadPokemons(){
        
        var pokemonArray:[Pokemon] = []
        let dpGR = DispatchGroup()
        
        for i in 1...20 {
            dpGR.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject { (response: DataResponse<Pokemon>) in
                
                let pokemon = response.result.value
                pokemonArray.append(pokemon!)
                dpGR.leave()
                
                }
                
            }
        dpGR.notify(queue: .main) {
            let sortArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
            self.delegate?.get20FirstPokemons(pokemons: sortArray)
        }
        
        }
    }
    

