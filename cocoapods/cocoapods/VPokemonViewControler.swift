//
//  VPokemonViewControler.swift
//  cocoapods
//
//  Created by AlejandroPardina on 29/11/17.
//  Copyright © 2017 AlejandroPardina. All rights reserved.
//

import UIKit

class VPokemonViewControler: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate{
    @IBOutlet weak var pokedexTable: UITableView!
    
    var pokemonArray:[Pokemon] = []
    var pokemonIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let service = PokemonService()
        service.delegate = self
        service.dowloadPokemons()

        // Do any additional setup after loading the view.
    }
    
    func get20FirstPokemons(pokemons: [Pokemon]) {
        pokemonArray = pokemons
        pokedexTable.reloadData()
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        cell.fillData(pokemon: pokemonArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        switch  section {
        case 0:
            return "Seccion 1"
        default:
            return "Seccion 2"
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        let pokemonDetail = segue.destination as! DetalleViewController
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonIndex = indexPath.row
        return indexPath
    }
    

}
