//
//  PokemonTableViewCell.swift
//  cocoapods
//
//  Created by AlejandroPardina on 3/1/18.
//  Copyright © 2018 AlejandroPardina. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heighLable: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func fillData (pokemon:Pokemon){
        nameLabel.text = pokemon.name
        heighLable.text = "\(pokemon.height ?? 0)"
        weightLabel.text = "\(pokemon.weight ?? 0)"
    }

    
}
